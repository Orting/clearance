-- MySQL dump 10.10
--
-- Host: localhost    Database: clearance
-- ------------------------------------------------------
-- Server version	5.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `factor`
--

DROP TABLE IF EXISTS `factor`;
CREATE TABLE `factor` (
  `f` int(11) default NULL,
  `batch` int(11) default NULL,
  `std_count` int(11) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
CREATE TABLE `patient` (
  `cpr` varchar(10) default NULL,
  `name` varchar(255) default NULL,
  `weight` int(11) default NULL,
  `std_count` int(11) default NULL,
  `nsamples` int(11) default NULL,
  `batch` int(11) default NULL,
  `clearance_norm` decimal(5,2) default NULL,
  `height` int(11) default NULL,
  `stamp` datetime default NULL,
  `factor` int(11) default NULL,
  `date` date default NULL,
  `metode` char(3) default NULL,
  `inj_time` datetime default NULL,
  `status` varchar(4) default NULL,
  `syringe` int(11) default NULL,
  `inj_before` decimal(6,4) default NULL,
  `inj_after` decimal(6,4) default NULL,
  `clearance` decimal(5,2) default NULL,
  `creatinin` decimal(5,3) default NULL,
  `accession_number` char(8) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `samples`
--

DROP TABLE IF EXISTS `samples`;
CREATE TABLE `samples` (
  `cpr` char(10) default NULL,
  `stamp` datetime default NULL,
  `counts` int(11) default NULL,
  `n` int(11) default NULL,
  `time` datetime default NULL,
  `flag` tinyint(4) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-10-03 12:41:23
